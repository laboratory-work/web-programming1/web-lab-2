package ru.lanolin.app.repo;

import lombok.Getter;
import ru.lanolin.app.domain.GraphDot;

import java.util.ArrayList;
import java.util.List;

public class PointsRepo {

	@Getter
	private static final List<GraphDot> Points = new ArrayList<>();

	public static void addNewDot(GraphDot dot) {
		Points.add(dot);
	}

	public static void clearList() {
		Points.clear();
	}

}
