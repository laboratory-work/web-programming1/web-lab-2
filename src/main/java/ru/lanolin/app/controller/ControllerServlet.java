package ru.lanolin.app.controller;

import ru.lanolin.app.util.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ControllerServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletContext().setAttribute("configLab", Configuration.getInstance());
		request.getRequestDispatcher("/views/index.jsp").forward(request, response);
	}
}
