package ru.lanolin.app.controller;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.io.IOUtils;
import ru.lanolin.app.domain.GraphDot;
import ru.lanolin.app.domain.Model;
import ru.lanolin.app.repo.PointsRepo;
import ru.lanolin.app.util.Switch;
import ru.lanolin.app.util.Utils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static ru.lanolin.app.util.Utils.OBJECT_MAPPER;

public class AreaCheckServlet extends HttpServlet {

	private Switch commandSwitch;

	@Override
	public void init() throws ServletException {
		super.init();

		commandSwitch = new Switch();
		commandSwitch.register("newPoint", this::newPointCommand);
		commandSwitch.register("changeR", this::changeRCommand);
		commandSwitch.register("clearDots", this::changeDotsCommand);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String data = IOUtils.toString(req.getReader());
		JsonNode node = OBJECT_MAPPER.readTree(data);
		String method = node.get("method").asText();

		try {
			commandSwitch.execute(method, req, res, node);
		}catch (IllegalStateException is) {
			is.printStackTrace();
			res.sendError(406, "Метод не поддерживается");
		}
	}

	private void newPointCommand(HttpServletRequest request, HttpServletResponse res, JsonNode node){
		long timeStart = System.currentTimeMillis();
		String x = node.get("X").asText();
		String y = node.get("Y").asText();
		String r = node.get("R").asText();
		int offset = (int) (node.get("offset").asLong() / 60);

		OffsetDateTime offsetDateTime = LocalDateTime.now().atOffset(ZoneOffset.ofHours(offset / 60));
		PointsRepo.addNewDot(new GraphDot(x, y, r, offsetDateTime.format(Utils.DATE_TIME_FORMATTER)));
		Model instance = new Model(PointsRepo.getPoints(), System.currentTimeMillis() - timeStart);

		try {
			request.setAttribute("model", instance);
			request.getRequestDispatcher("/views/areaCheck.jsp").forward(request, res);
		} catch (ServletException | IOException e) {
			e.printStackTrace();
		}
	}

	private void changeRCommand(HttpServletRequest request, HttpServletResponse res, JsonNode node){
		try {
			String r = node.get("R").asText();
			res.getWriter().println(Utils.generateJsonString("points", r, PointsRepo.getPoints()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void changeDotsCommand(HttpServletRequest request, HttpServletResponse res, JsonNode node) {
		try {
			PointsRepo.clearList();
			res.getWriter().println("Ok");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
