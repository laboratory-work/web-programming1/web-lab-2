package ru.lanolin.app.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Model implements Serializable {

	@Setter
	@Getter
	private List<GraphDot> graphDots;

	@Getter
	@Setter
	private long workTime;

	public Model(){
		graphDots = new ArrayList<>();
		workTime = 0;
	}

	public Model(List<GraphDot> graphDots, long workTime){
		this();
		this.graphDots = graphDots;
		this.workTime = workTime;
	}
}
