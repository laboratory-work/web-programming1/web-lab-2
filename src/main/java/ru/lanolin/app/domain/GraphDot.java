package ru.lanolin.app.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import ru.lanolin.app.interfaces.ICalculateArea;
import ru.lanolin.app.util.CalculateArea;

import java.io.Serializable;

@EqualsAndHashCode
@Getter
public class GraphDot implements Serializable {

	private static final ICalculateArea calcHit = CalculateArea::calcV2;

	private String x;
	private String y;
	private String r;
	private boolean hit;
	private String timeStamp;

	public GraphDot(String x, String y, String r, String timeStamp) {
		this.x = x;
		this.y = y;
		this.r = r;
		this.timeStamp = timeStamp;
		this.hit = calcHit.calc(x, y, r);
	}

	public boolean getHitAnotherR(String r) {
		return calcHit.calc(x, y, r);
	}
}