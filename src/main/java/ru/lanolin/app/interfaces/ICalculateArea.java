package ru.lanolin.app.interfaces;

@FunctionalInterface
public interface ICalculateArea {
	boolean calc(String xs, String ys, String rs);
}
