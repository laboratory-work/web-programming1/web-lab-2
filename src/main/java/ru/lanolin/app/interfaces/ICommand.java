package ru.lanolin.app.interfaces;

import com.fasterxml.jackson.databind.JsonNode;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@FunctionalInterface
public interface ICommand {
	void execute(HttpServletRequest req, HttpServletResponse res, JsonNode node);
}
