package ru.lanolin.app.util;

import lombok.extern.slf4j.Slf4j;
import ru.lanolin.app.util.ResettableStreamHttpServletRequest;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Slf4j
public abstract class AbstractFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig){
		log.info(String.format("Filter '%s' is loaded", getFilterName()));
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		ResettableStreamHttpServletRequest wrappedRequest = new ResettableStreamHttpServletRequest((HttpServletRequest) servletRequest);
		filter(wrappedRequest, servletResponse, filterChain);
	}

	@Override
	public void destroy() {
		log.info(String.format("Filter '%s' is destroy", getFilterName()));
	}

	public abstract String getFilterName();

	public abstract void filter(ResettableStreamHttpServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException;
}
