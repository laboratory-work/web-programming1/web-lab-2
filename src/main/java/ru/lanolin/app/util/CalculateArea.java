package ru.lanolin.app.util;

import java.math.BigDecimal;
import java.math.MathContext;

public final class CalculateArea {

	public static boolean calcV2(String xs, String ys, String rs){
		BigDecimal x = new BigDecimal(xs);
		BigDecimal y = new BigDecimal(ys);
		BigDecimal r = new BigDecimal(rs);

		int compareX = x.compareTo(BigDecimal.ZERO);
		int compareY = y.compareTo(BigDecimal.ZERO);

		if (compareX > 0 && compareY > 0) {
			return false;
		} else if (compareX <= 0 && compareY >= 0) {
			return x.pow(2).add(y.pow(2)).compareTo(r.pow(2)) <= 0;
		} else if (compareX <= 0) {
			return x.compareTo(r.negate().divide(BigDecimal.valueOf(2), MathContext.DECIMAL128)) >= 0
					&& y.compareTo(r.negate()) >= 0;
		} else {
			return y.compareTo(x.multiply(BigDecimal.valueOf(2)).subtract(r)) >= 0;
		}
	}

}
