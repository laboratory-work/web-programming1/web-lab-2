package ru.lanolin.app.util;

import com.fasterxml.jackson.databind.JsonNode;
import ru.lanolin.app.interfaces.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

public class Switch {

	private final HashMap<String, ICommand> commandMap;

	public Switch() {
		commandMap = new HashMap<>();
	}

	public void register(String name, ICommand command){
		commandMap.put(name, command);
	}

	public void execute(String name, HttpServletRequest req, HttpServletResponse res, JsonNode data){
		ICommand command = commandMap.get(name);
		if(command == null)
			throw new IllegalStateException("No find register command " + name);
		command.execute(req, res, data);
	}

	@Override
	protected void finalize() throws Throwable {
		commandMap.clear();
		super.finalize();
	}

	@Override
	public String toString() {
		StringBuilder out = new StringBuilder("Available command:");
		commandMap.keySet().forEach(s -> out.append(s).append(", "));
		return out.toString();
	}
}
