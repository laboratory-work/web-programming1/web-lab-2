package ru.lanolin.app.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import ru.lanolin.app.domain.GraphDot;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.stream.Collectors;

public class Utils {
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

    public static String generateJsonString(String nameRootObject, String newR, Collection<GraphDot> collection) throws JsonProcessingException {
        ObjectNode root = OBJECT_MAPPER.createObjectNode();
        ArrayNode array = OBJECT_MAPPER.createArrayNode();

        Collection<ObjectNode> nodes = collection.stream()
                .map(graphDot -> graphDot2ObjectNode(graphDot, newR))
                .collect(Collectors.toList());

        array.addAll(nodes);
        root.set(nameRootObject, array);

        return OBJECT_MAPPER.writeValueAsString(root);
    }

    private static ObjectNode graphDot2ObjectNode(GraphDot dot, String newR) {
        return OBJECT_MAPPER.createObjectNode()
                .put("X", dot.getX())
                .put("Y", dot.getY())
                .put("R", dot.getR())
                .put("Hit", dot.getHitAnotherR(newR));
    }
}
