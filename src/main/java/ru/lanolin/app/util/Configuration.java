package ru.lanolin.app.util;

import lombok.Getter;

import java.io.Serializable;

@Getter
public final class Configuration implements Serializable {

	private static Configuration instance = new Configuration();
	public static Configuration getInstance(){
		return instance;
	}

	private final Integer numberOfLaboratory = 2;
	private final String author = "Киселев Артем Олегович";
	private final String learningGroup = "P3214";
	private final Integer variant = 215790;
	private final Boolean isDeveloper;

	private Configuration() {
		String dev = System.getProperty("isDev");
		isDeveloper = Boolean.valueOf(dev == null ? "false" : "true");
	}
}
