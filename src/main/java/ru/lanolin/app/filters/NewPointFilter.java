package ru.lanolin.app.filters;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.io.IOUtils;
import ru.lanolin.app.util.AbstractFilter;
import ru.lanolin.app.util.ResettableStreamHttpServletRequest;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

import static ru.lanolin.app.util.Utils.OBJECT_MAPPER;

public class NewPointFilter extends AbstractFilter {

	@Override
	public String getFilterName() {
		return "NewPointFilter";
	}

	@Override
	public void filter(ResettableStreamHttpServletRequest request, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletResponse res = (HttpServletResponse) servletResponse;
		try {
			String data = IOUtils.toString(request.getReader());
			request.resetInputStream();
			JsonNode node = OBJECT_MAPPER.readTree(data);
			String method = node.get("method").asText();

			if ("newPoint".equalsIgnoreCase(method)) {
				if (!node.hasNonNull("X")) res.sendError(422, "Не найдено X значение");
				else if (!node.hasNonNull("Y")) res.sendError(422, "Не найдено Y значение");
				else if (!node.hasNonNull("R")) res.sendError(422, "Не найдено R значение");
				else if (!node.hasNonNull("offset")) res.sendError(422, "Не найдено offset значение");
				else {
					try {
						BigDecimal x = new BigDecimal(node.get("X").asText()).abs();
						if(x.compareTo(BigDecimal.valueOf(10)) >= 0) throw new NumberFormatException();
					} catch (NumberFormatException nfe) {
						res.sendError(406, "Формат вводимых переменной X неприемлем");
						return;
					}

					try {
						BigDecimal y = new BigDecimal(node.get("Y").asText());
						if(y.compareTo(BigDecimal.valueOf(10)) >= 0) throw new NumberFormatException();
					} catch (NumberFormatException nfe) {
						res.sendError(406, "Формат вводимых переменной Y неприемлем");
						return;
					}

					try {
						BigDecimal r = new BigDecimal(node.get("R").asText());
						if(r.compareTo(BigDecimal.valueOf(10)) >= 0) throw new NumberFormatException();
					} catch (NumberFormatException nfe) {
						res.sendError(406, "Формат вводимых переменной R неприемлем");
						return;
					}

					try {
						if(Math.abs(node.get("offset").asLong()) > 24*60) throw new NumberFormatException();
					} catch (NumberFormatException nfe) {
						res.sendError(406, "Формат вводимых переменной offset неприемлем");
						return;
					}
					filterChain.doFilter(request, servletResponse);
				}
			} else {
				filterChain.doFilter(request, servletResponse);
			}
		} catch (IOException io) {
			res.sendError(520, "Ошибка при чтении даных");
		}
	}
}
