package ru.lanolin.app.filters;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.io.IOUtils;
import ru.lanolin.app.util.AbstractFilter;
import ru.lanolin.app.util.ResettableStreamHttpServletRequest;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static ru.lanolin.app.util.Utils.OBJECT_MAPPER;

public class MethodFilter extends AbstractFilter {

	private static final List<String> availableMethod = Arrays.asList("newPoint", "changeR", "clearDots");

	@Override
	public String getFilterName() {
		return "MethodFilter";
	}

	@Override
	public void filter(ResettableStreamHttpServletRequest request, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletResponse res = (HttpServletResponse) servletResponse;
		try {
			String data = IOUtils.toString(request.getReader());
			request.resetInputStream();
			JsonNode node = OBJECT_MAPPER.readTree(data);

			if (node.hasNonNull("method")) {
				String method = node.get("method").asText().trim();

				if (availableMethod.contains(method)) {
					filterChain.doFilter(request, servletResponse);
				} else {
					res.sendError(405, "Метод не поддерживается");
				}

			} else {
				res.sendError(422, "Не найдено действие");
			}
		} catch (IOException io) {
			res.sendError(520, "Ошибка при чтении даных");
		}
	}
}
