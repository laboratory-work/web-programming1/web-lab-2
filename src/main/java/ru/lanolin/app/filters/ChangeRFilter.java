package ru.lanolin.app.filters;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.io.IOUtils;
import ru.lanolin.app.util.AbstractFilter;
import ru.lanolin.app.util.ResettableStreamHttpServletRequest;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

import static ru.lanolin.app.util.Utils.OBJECT_MAPPER;

public class ChangeRFilter extends AbstractFilter {

	@Override
	public String getFilterName() {
		return "ChangeRFilter";
	}

	@Override
	public void filter(ResettableStreamHttpServletRequest request, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletResponse res = (HttpServletResponse) servletResponse;
		try {
			String data = IOUtils.toString(request.getReader());
			request.resetInputStream();
			JsonNode node = OBJECT_MAPPER.readTree(data);
			String method = node.get("method").asText();

			if ("changeR".equalsIgnoreCase(method)) {
				if (!node.hasNonNull("R")) res.sendError(422, "Не найдено R значение");
				else {
					try {
						new BigDecimal(node.get("R").asText());
					} catch (NumberFormatException nfe) {
						res.sendError(406, "Формат вводимых переменной R неприемлем");
						return;
					}
					filterChain.doFilter(request, servletResponse);
				}
			} else {
				filterChain.doFilter(request, servletResponse);
			}
		} catch (IOException io) {
			res.sendError(520, "Ошибка при чтении даных");
		}
	}
}
