import Graphics from './graphics.js';

//------------------------------------------------------------------------------------//
export const selectR = $('#selectR');
const inputY = $("#textY");
const tooltip = $("#tooltipId");
const form = $("#sendD");
const submitB = $("#submit");
const reloadB = $("#reload");
const result = $(".result");

export const funcErrorDefault = (req) => result.attr("srcdoc", req.responseText);
export const funcSuccessDefault = (req) => {
    result.attr("srcdoc", req.responseText);
    graphics.drawPoint(selectR.val());
};

const graphics = new Graphics("#canvas_1", "#canvas_2");
//------------------------------------------------------------------------------------//

tooltipHidden(true, '');
submitB.prop('disabled', true);

selectR.val(1);
selectR.on('change', () => graphics.changeGraphicsRadius(selectR.val()));

$('#radioX5').prop("checked", true);

setInputFilter(document.getElementById("textY"), (value) => /^-?\d*[.,]?\d*$/.test(value));
inputY.on('change', () => checkY(inputY.val().replace(',', '.')));

form.on('submit', function (event) {
    event.preventDefault();
    inputY.val(inputY.val().replace(",", '.'));
    if (!checkY(inputY.val())) return;

    let data = {'method': 'newPoint', "offset": new Date().getTimezoneOffset()};
    $.each(form.serializeArray(), (i, element) => data[element.name] = Number(element.value));

    sendDate('POST', data, funcSuccessDefault, funcErrorDefault);
});

reloadB.on('click', (event) => {
    let data = {'method': 'clearDots'};

    let func = (req) => {
        if(req.responseText.trim() ===  'Ok'){
            result.attr("srcdoc", '');
            graphics.drawArea(selectR.val());
        }
    };

    sendDate('POST', data, func, funcErrorDefault);
});

graphics.drawBackground();
graphics.drawArea(selectR.val());

//------------------------------------------------------------------------------------//
export function sendDate(method = 'POST', date, func, funcError) {
    let req = new XMLHttpRequest();
    req.open(method, "areaCheck", true);
    req.setRequestHeader("Content-type", "application/json");
    req.onreadystatechange = () => {
        if (req.readyState === 4) {
            if (req.status === 200) {
                if (func) func(req);
            } else if(req.status >= 400){
                if (funcError) funcError(req);
            }
        }
    };
    req.send(JSON.stringify(date));
}

function setInputFilter(textBox, inputFilter) {
    ["input", 'keyup'].forEach(function (event) {
        textBox.addEventListener(event, function (e) {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
                tooltipHidden(true, "");
                checkY(inputY.val().replace(",", '.'));
            } else if (this.hasOwnProperty("oldValue")) {
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                tooltipHidden(false, "Введете цифры из диапазона 0..9");
            }
        });
    });
}

function checkY(y) {
    let flag = false;
    if (y !== "") {
        if (isNaN(y) && !Number.isFinite(Number(y)) || /^\./.test(y) || /\.$/.test(y) || /-0$/.test(y)) {
            tooltipHidden(false, "Некорректное значение Y");
        } else {
            const yB = Big(y);
            const yMax = Big(3);
            const yMin = new Big(-5);
            if (yB.gt(yMin) && yB.lt(yMax)) {
                flag = true;
                tooltipHidden(true, "");
            } else {
                tooltipHidden(false, 'Выход за пределы диапазона (-5;3)');
            }
        }
    } else {
        tooltipHidden(false, 'Не введено значение Y');
    }
    return flag;
}

function tooltipHidden(flag, text) {
    tooltip.html(text);
    if (flag) {
        tooltip.css("visibility", "hidden");
        submitB.prop('disabled', false);
    } else {
        tooltip.css("visibility", "visible");
        submitB.prop('disabled', true);
    }
}