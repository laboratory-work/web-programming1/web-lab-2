import {selectR, sendDate, funcSuccessDefault, funcErrorDefault} from './main.js';
import JCanvas from 'jcanvas';

export default class Graphics {
    delta = 5;
    radius = [1, 2, 3, 4, 5];

    constructor(back, front){
        JCanvas($, window);
        this.background = $(back);
        this.foreground = $(front);

        this.width = $(back).width();
        this.height = $(back).height();

        this.widthHalf = this.width / 2;
        this.heightHalf = this.height / 2;

        this.oneLineWidth = (this.widthHalf - this.delta * 3) / this.countLines;
        this.oneLineHeight = (this.heightHalf - this.delta * 3) / this.countLines;
    }

    get countLines() {
        return this.radius.length;
    }

    drawBackground() {
        this.background.drawLine({
            strokeStyle: '#000',
            strokeWidth: 3,
            startArrow: true,
            arrowRadius: this.delta * 2,
            arrowAngle: 60,
            x1: this.widthHalf, y1: this.delta,
            x2: this.widthHalf, y2: this.height
        }).drawLine({
            strokeStyle: '#000',
            strokeWidth: 3,
            endArrow: true,
            arrowRadius: this.delta * 2,
            arrowAngle: 60,
            x1: 0, y1: this.heightHalf,
            x2: this.width - this.delta, y2: this.heightHalf
        });

        $.each(this.radius, (i, element) => {
            this.background
                .drawLine(this.getHath(
                    this.widthHalf - this.oneLineWidth * (i + 1),
                    this.widthHalf - this.oneLineWidth * (i + 1),
                    this.heightHalf - this.delta,
                    this.heightHalf + this.delta
                ))
                .drawText(this.getText(
                    this.widthHalf - this.oneLineWidth * (i + 1) - this.delta / 2,
                    this.heightHalf + this.delta * 3,
                    -element
                ))
                .drawLine(this.getHath(
                    this.widthHalf + this.oneLineWidth * (i + 1),
                    this.widthHalf + this.oneLineWidth * (i + 1),
                    this.heightHalf - this.delta,
                    this.heightHalf + this.delta
                ))
                .drawText(this.getText(
                    this.widthHalf + this.oneLineWidth * (i + 1),
                    this.heightHalf + this.delta * 3,
                    element
                ))
                .drawLine(this.getHath(
                    this.widthHalf - this.delta,
                    this.widthHalf + this.delta,
                    this.heightHalf - this.oneLineHeight * (i + 1),
                    this.heightHalf - this.oneLineHeight * (i + 1)
                ))
                .drawText(this.getText(
                    this.widthHalf - this.delta * 2,
                    this.heightHalf - this.oneLineHeight * (i + 1),
                    element
                ))
                .drawLine(this.getHath(
                    this.widthHalf - this.delta,
                    this.widthHalf + this.delta,
                    this.heightHalf + this.oneLineHeight * (i + 1),
                    this.heightHalf + this.oneLineHeight * (i + 1)
                ))
                .drawText(this.getText(
                    this.widthHalf - this.delta * 2,
                    this.heightHalf + this.oneLineHeight  * (i + 1),
                    -element
                ));
        });

        $(window).on('mouseup', (e) => { this.sendNewPoint(e); });
    }

    sendNewPoint(e){
        let x = e.clientX - this.foreground.offset().left - this.widthHalf + document.documentElement.scrollLeft;
        let y = -(e.clientY - this.foreground.offset().top - this.heightHalf + document.documentElement.scrollTop);

        if (x < -this.widthHalf || x > this.widthHalf) {
            return;
        }
        if (y < -this.heightHalf || y > this.heightHalf) {
            return;
        }

        x = x / this.oneLineWidth;
        y = y / this.oneLineHeight;

        let date = {
            "method": "newPoint",
            "offset": new Date().getTimezoneOffset(),
            "X": x,
            "Y": y,
            "R": selectR.val()
        };

        sendDate('POST', date, funcSuccessDefault, funcErrorDefault);
    }

    drawArea(r = 1) {
        this.foreground.clearCanvas();
        this.foreground
            .drawSlice({
                strokeStyle: 'rgba(0,0,255, 0.2)',
                strokeWidth: 1,
                fillStyle: 'rgba(0,0,255, 0.2)',
                radius: (this.heightHalf - this.delta * 3) / this.countLines * r,
                x: this.widthHalf, y: this.heightHalf,
                start: -90, end: 0,
            })
            .drawLine({
                strokeStyle: 'rgba(0,0,255, 0.2)',
                strokeWidth: 1,
                fillStyle: 'rgba(0,0,255, 0.2)',
                closed: true,
                x1: this.widthHalf, y1: this.heightHalf,
                x2: this.widthHalf, y2: this.heightHalf + this.oneLineHeight * r,
                x3: this.widthHalf + this.oneLineWidth * r / 2, y3: this.heightHalf,

            })
            .drawRect({
                strokeStyle: 'rgba(0,0,255, 0.2)',
                strokeWidth: 1,
                fillStyle: 'rgba(0,0,255, 0.2)',
                height: this.oneLineHeight * r,
                width: this.oneLineWidth * r / 2,
                x: this.widthHalf - this.oneLineWidth * r / 4,
                y: this.heightHalf + this.oneLineHeight * r / 2
            });
    }

    changeGraphicsRadius(radius) {
        this.drawArea(radius);
        this.drawPoint(radius);
    }

    drawPoint(radius) {
        let data = {"method": "changeR", "R": radius};
        let func = (req) => {
            $.each(JSON.parse(req.responseText)['points'], (i, element) => {
                let x = element['X'] * this.oneLineWidth + this.widthHalf;
                let y = -element['Y'] * this.oneLineHeight + this.heightHalf;
                this.foreground.drawEllipse(this.getPoint(x, y, element['Hit']));
            });
        };
        sendDate('POST', data, func, funcErrorDefault);
    }

    getPoint(x, y, hit) {
        return {
            fillStyle: hit ? 'rgb(0,255,0)' : 'rgb(255,0,0)',
            strokeStyle: hit ? 'rgb(0,255,0)' : 'rgb(255,0,0)',
            strokeWidth: 1,
            x: x, y: y,
            width: 3, height: 3
        }
    }

    getHath(x1, x2, y1, y2) {
        return {
            strokeStyle: '#000',
            strokeWidth: 2,
            x1: x1, y1: y1,
            x2: x2, y2: y2
        };
    }

    getText(x, y, text) {
        return {
            fillStyle: '#000',
            strokeStyle: '#000',
            strokeWidth: 1,
            fontSize: 10,
            fontFamily: 'Arial',
            x: x, y: y,
            text: text
        };
    }

}

// const canvas_1 = $("#canvas_1");
// const canvas_2 = $("#canvas_2");
//
// const width = canvas_1.width();
// const height = canvas_1.height();
//
// const widthHalf = width / 2;
// const heightHalf = height / 2;
// const delta = 5;
//
// const radiuses = [1, 2, 3, 4, 5];
//
// const countLines = radiuses.length;
//
// const oneLineWidth = (widthHalf - delta * 3) / countLines;
// const oneLineHeight = (heightHalf - delta * 3) / countLines;
//
// function drawBackground() {
//     canvas_1.drawLine({
//         strokeStyle: '#000',
//         strokeWidth: 3,
//         startArrow: true,
//         arrowRadius: delta * 2,
//         arrowAngle: 60,
//         x1: widthHalf, y1: delta,
//         x2: widthHalf, y2: height
//     }).drawLine({
//         strokeStyle: '#000',
//         strokeWidth: 3,
//         endArrow: true,
//         arrowRadius: delta * 2,
//         arrowAngle: 60,
//         x1: 0, y1: heightHalf,
//         x2: width - delta, y2: heightHalf
//     });
//
//     $.each(radiuses, (i, element) => {
//         canvas_1
//             .drawLine(getHath(widthHalf - oneLineWidth * (i + 1), widthHalf - oneLineWidth * (i + 1), heightHalf - delta, heightHalf + delta))
//             .drawText(getText(widthHalf - oneLineWidth * (i + 1) - delta / 2, heightHalf + delta * 3, -element))
//             .drawLine(getHath(widthHalf + oneLineWidth * (i + 1), widthHalf + oneLineWidth * (i + 1), heightHalf - delta, heightHalf + delta))
//             .drawText(getText(widthHalf + oneLineWidth * (i + 1), heightHalf + delta * 3, element))
//
//             .drawLine(getHath(widthHalf - delta, widthHalf + delta, heightHalf - oneLineHeight * (i + 1), heightHalf - oneLineHeight * (i + 1)))
//             .drawText(getText(widthHalf - delta * 2, heightHalf - oneLineHeight * (i + 1), element))
//             .drawLine(getHath(widthHalf - delta, widthHalf + delta, heightHalf + oneLineHeight * (i + 1), heightHalf + oneLineHeight * (i + 1)))
//             .drawText(getText(widthHalf - delta * 2, heightHalf + oneLineHeight  * (i + 1), -element));
//     });
//
//     $(window).on('mouseup', function (e) {
//         let x = e.clientX - canvas_2.offset().left - widthHalf + document.documentElement.scrollLeft;
//         let y = -(e.clientY - canvas_2.offset().top - heightHalf + document.documentElement.scrollTop);
//
//         if (x < -widthHalf || x > widthHalf) {
//             return;
//         }
//         if (y < -heightHalf || y > heightHalf) {
//             return;
//         }
//
//         x = x / oneLineWidth;
//         y = y / oneLineHeight;
//
//         let date = {
//             "method": "newPoint",
//             "offset": new Date().getTimezoneOffset(),
//             "X": x,
//             "Y": y,
//             "R": selectR.val()
//         };
//
//         sendDate('POST', date, funcSuccessDefault, funcErrorDefault);
//     });
// }

// function drawArea(r = 1) {
//     canvas_2.clearCanvas();
//     canvas_2
//         .drawSlice({
//             strokeStyle: 'rgba(0,0,255, 0.2)',
//             strokeWidth: 1,
//             fillStyle: 'rgba(0,0,255, 0.2)',
//             radius: (heightHalf - delta * 3) / countLines * r,
//             x: widthHalf, y: heightHalf,
//             start: -90, end: 0,
//         })
//         .drawLine({
//             strokeStyle: 'rgba(0,0,255, 0.2)',
//             strokeWidth: 1,
//             fillStyle: 'rgba(0,0,255, 0.2)',
//             closed: true,
//             x1: widthHalf, y1: heightHalf,
//             x2: widthHalf, y2: heightHalf + oneLineHeight * r,
//             x3: widthHalf + oneLineWidth * r / 2, y3: heightHalf,
//
//         })
//         .drawRect({
//             strokeStyle: 'rgba(0,0,255, 0.2)',
//             strokeWidth: 1,
//             fillStyle: 'rgba(0,0,255, 0.2)',
//             height: oneLineHeight * r,
//             width: oneLineWidth * r / 2,
//             x: widthHalf - oneLineWidth * r / 4,
//             y: heightHalf + oneLineHeight * r / 2
//         });
// }
//
// function changeGraphicsRadius(radius) {
//     drawArea(radius);
//     drawPoint(radius);
// }
//
// function drawPoint(radius) {
//     let data = {"method": "changeR", "R": radius};
//     let func = (req) => {
//         $.each(JSON.parse(req.responseText)['points'], (i, element) => {
//             let x = element['X'] * oneLineWidth + widthHalf;
//             let y = -element['Y'] * oneLineHeight + heightHalf;
//             canvas_2.drawEllipse(getPoint(x, y, element['Hit']));
//         });
//     };
//     sendDate('POST', data, func, funcErrorDefault);
// }
//
// function getPoint(x, y, hit) {
//     return {
//         fillStyle: hit ? 'rgb(0,255,0)' : 'rgb(255,0,0)',
//         strokeStyle: hit ? 'rgb(0,255,0)' : 'rgb(255,0,0)',
//         strokeWidth: 1,
//         x: x, y: y,
//         width: 3, height: 3
//     }
// }
//
// function getHath(x1, x2, y1, y2) {
//     return {
//         strokeStyle: '#000',
//         strokeWidth: 2,
//         x1: x1, y1: y1,
//         x2: x2, y2: y2
//     };
// }
//
// function getText(x, y, text) {
//     return {
//         fillStyle: '#000',
//         strokeStyle: '#000',
//         strokeWidth: 1,
//         fontSize: 10,
//         fontFamily: 'Arial',
//         x: x, y: y,
//         text: text
//     };
// }