<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <jsp:useBean id="configLab" class="ru.lanolin.app.util.Configuration" scope="application"/>
    <meta charset="UTF-8">
    <title>Лабораторная ${configLab.numberOfLaboratory}</title>
<%--    <script type="text/javascript" src="js/lib/jquery-3.4.1.min.js"></script>--%>
<%--    <script type="text/javascript" src="js/lib/jcanvas.js"></script>--%>
<%--    <script type="text/javascript" src="js/lib/big.min.js"></script>--%>
    <link rel="stylesheet" href="styles/w3.css">
    <link rel="stylesheet" href="styles/style.css">
</head>
<body>
<header>
    <h1 style="margin-top: 5px; margin-bottom: -5px;">
        <button class="w3-btn w3-border-0" id="reload">Попадание в область</button>
    </h1>
    ${configLab.author} группа ${configLab.learningGroup} <br/>
    Вариант ${configLab.variant}
</header>

<article class="work">
    <form method="POST" id="sendD">
        <table border="1px">
            <tr style="height: 45px">
                <th>Поле</th>
                <th>Значение</th>
                <td class="graphic" rowspan="5">
                    <canvas id="canvas_1" width="250px" height="250px"></canvas>
                    <canvas id="canvas_2" width="250px" height="250px"></canvas>
                </td>
            </tr>
            <tr>
                <td>X</td>
                <td>
                    <div class="w3-container">
                        <input id="radioX1" class="w3-radio" name="X" type="radio" value="-2" /><label for="radioX1">-2</label>
                        <input id="radioX2" class="w3-radio" name="X" type="radio" value="-1.5"/><label for="radioX2">-1.5</label>
                        <input id="radioX3" class="w3-radio" name="X" type="radio" value="-1"/><label for="radioX3">-1</label>
                        <input id="radioX4" class="w3-radio" name="X" type="radio" value="-0.5"/><label for="radioX4">-0.5</label>
                        <input id="radioX5" class="w3-radio" name="X" type="radio" value="0" checked/><label for="radioX5">0</label> <br>
                        <input id="radioX6" class="w3-radio" name="X" type="radio" value="0.5"/><label for="radioX6">0.5</label>
                        <input id="radioX7" class="w3-radio" name="X" type="radio" value="1"/><label for="radioX7">1</label>
                        <input id="radioX8" class="w3-radio" name="X" type="radio" value="1.5"/><label for="radioX8">1.5</label>
                        <input id="radioX9" class="w3-radio" name="X" type="radio" value="2"/><label for="radioX9">2</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td><label for="textY">Y</label></td>
                <td>
                    <input id="textY" class="w3-input" type="text" name="Y" placeholder="от -5 до 3" required/>
                    <div class="tooltip"><span class="tooltiptext" id="tooltipId">Erropr</span></div>
                </td>
            </tr>
            <tr>
                <td><label for="selectR">R</label></td>
                <td>
                    <select id="selectR" class="w3-select w3-border-green w3-border w3-round-xlarge" name="R">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" class="w3-btn w3-white w3-border w3-border-green w3-round-xlarge" id="submit" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <iframe class="result" name="result"></iframe>
                </td>
            </tr>
        </table>
    </form>
</article>

<footer>Copyright. All rights reserved. 2019</footer>
</body>
<%--<script type="text/javascript" src="js/graphics.js"></script>--%>
<%--<script type="text/javascript" src="js/main.js"></script>--%>
<script src="<c:if test="${configLab.isDeveloper}">http://localhost:8000/main.js</c:if><c:if test="${!configLab.isDeveloper}">/static.js/main.js</c:if>"></script>
</html>
