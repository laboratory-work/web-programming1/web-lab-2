<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Результат проверки</title>
    <style>
        .success {
            color: green;
        }

        .wrong {
            color: red;
        }

        td {
            border-top: 1px solid #e8edff;
            padding: 5px 5px;
            text-align: center;
        }

        tr:hover td {
            background: #e8edff;
        }

        td {
            width: 130px;
        }
    </style>
</head>
<body>
<jsp:useBean id="model" scope="request" beanName="model" type="ru.lanolin.app.domain.Model"/>

<p id='time2'>Время работы скрипта: <jsp:getProperty name="model" property="workTime"/> мс<br></p>

<table>
    <tr>
        <th>X</th>
        <th>Y</th>
        <th>R</th>
        <th>Hit</th>
        <th>Время</th>
    </tr>
    <c:forEach items="${model.graphDots}" var="dot">
        <tr>
            <td>${dot.x}</td>
            <td>${dot.y}</td>
            <td>${dot.r}</td>
            <td class="<c:if test="${dot.hit}">success</c:if><c:if test="${!dot.hit}">wrong</c:if>">${dot.hit}</td>
            <td>${dot.timeStamp}</td>
        </tr>
    </c:forEach>
</table>

<script>
    window.scrollTo(0, document.body.scrollHeight);
</script>
</body>
</html>
