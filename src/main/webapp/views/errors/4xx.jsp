<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>${requestScope['javax.servlet.error.status_code']}</title>
</head>
<body>

<h1>Ошибка</h1>

Status: ${requestScope['javax.servlet.error.status_code']} <br>
Type: ${requestScope['javax.servlet.error.message']} <br>
Time: <%= new Date() %> <br>

</body>
</html>
